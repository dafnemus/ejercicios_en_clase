from flask import Flask, send_from_directory
from flask import render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('css', path)	


@app.route('/images/<path:path>')
def send_images(path):
    return send_from_directory('images', path)	    