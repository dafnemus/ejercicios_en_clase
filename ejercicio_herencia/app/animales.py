class Animal(object):

    def cantidad_de_patas(self):
        # bla
        # bla
        # bla
        return 2

    def hablar(self):
        return "hola"

class Persona(Animal):
    def hablar(self):
        return "bla"

class Perro(Animal):

    def hablar(self):
        return self.ladrar()

    def ladrar(self):
        return "guau"

    def cantidad_de_patas(self):
        print("soy perro")
        return 2 + 2


class PerroLabrador(Perro):
    pass

class Paloma (Animal):

    def hablar(self):
        pass

    def volar(self):
        return "mueve las alas"


class Contador:

    @staticmethod
    def contar_patas_de(lista):
        suma = 0
        for x in lista:
            suma += x.cantidad_de_patas()
        return suma
