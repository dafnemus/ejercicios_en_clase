class SalaDeEspera1(list):

    def llega_nuevo_paciente(self, paciente):
        self.append(paciente)

    def siguiente_paciente(self):
        if len(self) == 0:
            return None
        paciente = self[0]
        self.remove(paciente)
        return paciente

class SalaDeEspera2:

    def __init__(self):
        self.__lista = []

    def llega_nuevo_paciente(self, paciente):
        self.__lista.append(paciente)

    def siguiente_paciente(self):
        if len(self.__lista) == 0:
            return None
        paciente = self.__lista[0]
        self.__lista.remove(paciente)
        return paciente

class SalaDeEspera:

    def __init__(self):
        self.__cola = Cola()

    def llega_nuevo_paciente(self, paciente):
        self.__cola.encolar(paciente)

    def siguiente_paciente(self):
        return self.__cola.desencolar()

class Cola():

    def __init__(self):
        self.elementos = []

    def encolar(self, nuevo_elemento):
        self.elementos.append(nuevo_elemento)

    def esta_vacia(self):
        return len(self.elementos) == 0

    def desencolar(self):
        if len(self.elementos) >= 1:
            return self.elementos.pop(0)
        return None

class Pila:
    def __init__(self):
        self.elementos = []

    def esta_vacia(self):
        return len(self.elementos) == 0

    def apilar(self, elemento):
        self.elementos.append(elemento)

    def desapilar(self):
        if self.esta_vacia():
            return None

        return self.elementos.pop(-1)

class PilaSobreCola:

    def __init__(self):
        self.__cola_de_elementos = Cola()

    def esta_vacia(self):
        return self.__cola_de_elementos.esta_vacia()

    def apilar(self, elemento):
        self.__cola_de_elementos.encolar(elemento)

    def desapilar(self):
        cola_aux = Cola()
        elemento = None
        while not self.__cola_de_elementos.esta_vacia():
            elemento = self.__cola_de_elementos.desencolar()
            if not self.__cola_de_elementos.esta_vacia():
                cola_aux.encolar(elemento)
        self.__cola_de_elementos = cola_aux
        return elemento
