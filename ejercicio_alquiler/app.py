# python app.py <fecha_alquiler> <cuit> <tipo_alquiler> <parametros_alquiler>

import sys
from app.alquileres import AlquilerPorHora, AlquilerPorDia

fecha_alquiler = sys.argv[1]
cuit = sys.argv[2]
tipo_alquiler = sys.argv[3]
parametro_alquiler = int(sys.argv[4])

# logica de calculo
#alquiler = Alquiler.crear(fecha_alquiler, cuit, tipo_alquiler)
#monto_facturado = alquiler.facturar()

if tipo_alquiler == "h":
    alquiler = AlquilerPorHora(cuit, parametro_alquiler)
else:
    alquiler = AlquilerPorDia(cuit, parametro_alquiler)

importe_facturado = alquiler.facturar()

print("Importe: {}".format(importe_facturado))