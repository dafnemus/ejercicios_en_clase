from app.alquileres import AlquilerPorHora
from app.alquileres import AlquilerPorDia

cuit_persona = "20112223336"
cuit_empresa = "26112223336"

def test_alquiler_por_hora():
    alquiler = AlquilerPorHora(cuit=cuit_persona, horas=3)
    assert alquiler.facturar() == 300

def test_alquiler_por_hora_con_descuento_empresa():
    alquiler = AlquilerPorHora(cuit=cuit_empresa, horas=1)
    assert alquiler.facturar() == 95

def test_alquiler_por_dia():
    alquiler = AlquilerPorDia(cuit=cuit_persona, dias=1)
    assert alquiler.facturar() == 2000

def test_alquiler_por_dia_con_descuento_empresa():
    alquiler = AlquilerPorDia(cuit=cuit_empresa, dias=1)
    assert alquiler.facturar() == 1900