from app.mate import Mate
import pytest


def test_mate_se_crea_vacio():
    mate = Mate(20)
    assert mate.esta_vacio() is True


def test_mate_cuando_lo_cebo_esta_lleno():
    mate = Mate(20)
    mate.cebar()
    assert mate.esta_vacio() is False

def test_mate_cuando_lo_bebo_esta_vacio():
    mate = Mate(20)
    mate.cebar()
    mate.beber()
    assert mate.esta_vacio() is True

def test_mate_cuando_lo_cebo_disnuye_cebadas_disponibles():
    mate = Mate(20)
    mate.cebar()
    assert mate.cebadas_disponibles() == 19


def test_mate_no_se_puede_cebar_cuando_esta_lleno():
    mate = Mate(20)
    mate.cebar()
    with pytest.raises(ValueError):
        mate.cebar()

def test_mate_no_se_puede_beber_cuando_esta_vacio():
    mate = Mate(20)
    with pytest.raises(ValueError):
        mate.beber()

# el mate tiene X cebadas porque se llava la yerba
# si lo cebo cuando ya no tiene cebadas => error
