from flask import Flask, jsonify, request
from flask.helpers import make_response


api = Flask(__name__)
contador_de_sumas = 0

# POST /sumar { x: y: }  => 200 { resultado: }
@api.route('/sumar', methods=['POST'])
def sumar():
    datos = request.get_json()
    x = datos.get('x')
    y = datos.get('y')
    global contador_de_sumas
    contador_de_sumas = contador_de_sumas + 1
    respuesta = { 'resultado': x + y, 'total_sumas': contador_de_sumas }
    return make_response(jsonify(respuesta), 200)
