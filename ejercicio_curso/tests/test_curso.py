from app.curso import Estudiante
from app.curso import Curso
from datetime import date
import pytest


def crear_estudiante():
    completo_secundario = True
    fecha_nacimiento = date(2000,1,1)
    estudiante = Estudiante("juan", "22222222",fecha_nacimiento, completo_secundario)
    return estudiante

def test_estudiante_es_mayor_de_edad():
    completo_secundario = True
    fecha_nacimiento = date(2000,1,1)
    estudiante = Estudiante("juan", "23444555", fecha_nacimiento, completo_secundario)

    assert estudiante.es_mayor_de_edad() is True

def test_curso_solo_admite_estudiantes_mayores():

    cupo = 10
    curso = Curso(cupo)
    curso.agregar_estudiante(crear_estudiante())
    curso.agregar_estudiante(crear_estudiante())

    assert curso.cantidad_de_estudiantes_nacidos_en(2000) == 2


def test_curso_no_admite_estudiantes_menores():
    completo_secundario = True
    estudiante = crear_estudiante()
    estudiante.fecha_nacimiento = date(2020,1,1)

    cupo = 10
    curso = Curso(cupo)

    with pytest.raises(ValueError):
        curso.agregar_estudiante(estudiante)

    assert curso.cantidad_de_estudiantes() == 0

def test_curso_no_puede_exceder_su_cupo():
    cupo = 1
    curso = Curso(cupo)
    curso.agregar_estudiante(crear_estudiante())

    with pytest.raises(ValueError):
        curso.agregar_estudiante(crear_estudiante())
