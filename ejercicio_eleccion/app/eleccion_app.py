import sys
import csv
from eleccion_modelo import Eleccion

eleccion = Eleccion()

nombre_archivo_configuracion = sys.argv[1]
nombre_archivo_votos = sys.argv[2]
nombre_archivo_resultado = sys.argv[3]
provincias = []

with open(nombre_archivo_configuracion, "r") as configuracion:
    for linea in configuracion:
        nombre_provincia = linea.rstrip("\n")
        provincias.append(nombre_provincia)
        eleccion.agregar_provincia(nombre_provincia)

with open(nombre_archivo_votos, "r") as votos:
    archivo_csv_votos = csv.reader(votos)
    for provincia, candidato in archivo_csv_votos:
        eleccion.agregar_voto(provincia, candidato)

# para cada provincia en eleccion.provincias
with open(nombre_archivo_resultado, "w") as resultado:
    archivo_csv_resultado = csv.writer(resultado)
    for provincia in provincias:
        resultado = eleccion.resultado_en(provincia)
        archivo_csv_resultado.writerow([provincia, resultado])